# Flujo de trabajo para la entrega de tareas

![https://drive.google.com/open?id=1_fhDH4NX_2kvMWoRupcdpaOqAATsbw9s](img/000-workflow.png "")

## Acceder al repositorio principal

+ Abrir la URL del [repositorio de tareas para la materia][repositorio-tareas]

|                               |
|:-----------------------------:|
| ![](img/001-Main_repo.png "") |

+ [Iniciar sesión en GitLab][gitlab-login]

|                             |
|:---------------------------:|
| ![](img/002-Sign_in.png "") |

--------------------------------------------------------------------------------

## Haz `fork` del repositorio principal

+ Dar clic en el botón de `fork` para crear una copia del repositorio `tareas-so` en tu cuenta de usuario

|                               |
|:-----------------------------:|
| ![](img/003-Fork_repo.png "") |

+ Selecciona tu usuario, marca el repositorio como **público** y espera a que el _fork_ se complete

|                                      |
|:------------------------------------:|
| ![](img/004-Fork_repo-settings.png "") |

--------------------------------------------------------------------------------

## Da permisos en tu repositorio

+ Accede a la sección **members** de tu repositorio

|                                       |
|:-------------------------------------:|
| ![](img/006-Fork_members-menu.png "") |

+ Busca el nombre de usuario y da permisos de `maintainer` a los demás integrantes de tu equipo

|                                            |
|:------------------------------------------:|
| ![](img/007-Fork_grant-permissions.png "") |

>>>
Esto no es necesario para la `tarea-0`
>>>

--------------------------------------------------------------------------------

## Clona el repositorio

+ Accede a la URL del repositorio `tareas-so` asociado a **tu cuenta de usuario**

```
https://gitlab.com/USUARIO/tareas-so.git
```

+ Obten la URL de tu repositorio `tareas-so` y bájalo a tu equipo con `git clone`

|                                               |
|:---------------------------------------------:|
| ![](img/008-Fork_successful-clone_URL.png "") |

```
$ git clone https://gitlab.com/USUARIO/tareas-so.git
Cloning into 'tareas-so'...
remote: Enumerating objects: 31, done.
remote: Counting objects: 100% (31/31), done.
remote: Compressing objects: 100% (27/27), done.
remote: Total 31 (delta 11), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (31/31), 4.95 KiB | 2.48 MiB/s, done.
Resolving deltas: 100% (11/11), done.
```

+ Lista el contenido del repositorio y de la carpeta `docs`

```
$ cd tareas-so/
$ ls -lA
total 20
drwxr-xr-x 12 tonejito staff 384 Sep 28 00:01 .git
drwxr-xr-x  3 tonejito staff  96 Sep 28 00:01 docs
-rw-r--r--  1 tonejito staff 728 Sep 28 00:01 .gitignore
-rw-r--r--  1 tonejito staff 958 Sep 28 00:01 .gitlab-ci.yml
-rw-r--r--  1 tonejito staff 262 Sep 28 00:01 Makefile
-rw-r--r--  1 tonejito staff 262 Sep 28 00:01 README.md
-rw-r--r--  1 tonejito staff 264 Sep 28 00:01 mkdocs.yml
-rw-r--r--  1 tonejito staff 203 Sep 28 00:01 requirements.txt

$ tree -a docs/
docs/
├── README.md -> ../README.md
├── entrega
│   ├── README.md
│   └── tarea-0
│       └── README.md
│       └── ...
└── workflow
    ├── README.md
    └── img
        ├── .gitkeep
        ├── 000-workflow.png
        └── ...
```

--------------------------------------------------------------------------------

## Crear rama de trabajo

+ Revisa que de manera inicial te encuentres en la rama `entregas`

```
$ git branch
* entregas
```

+ Crea una rama con tu nombre utilizando `git checkout`

```
$ git checkout -b AndresHernandez
Switched to a new branch 'AndresHernandez'
```

+ Revisa que hayas cambiado a la rama con tu nombre
    - Debe tener el prefijo `*`

```
$ git branch
* AndresHernandez
  entregas
```

--------------------------------------------------------------------------------

## Agregar carpeta personal

+ Accede al repositorio y crea una carpeta con tu nombre bajo la ruta `docs/entrega`

```
$ mkdir -vp docs/entrega/tarea-0/AndresHernandez/
mkdir: created directory ‘docs/entrega/tarea-0/AndresHernandez/’
```

>>>
**Nota:**
Dentro de esa carpeta es donde debes poner los archivos de esta tarea.
Cada práctica y tarea tendrá su propia carpeta y debes seguir la estructura de directorios propuesta.
>>>

--------------------------------------------------------------------------------

### Agregar una imagen

+ Crea el directorio de imagenes

```
$ mkdir -vp docs/entrega/tarea-0/AndresHernandez/img/
mkdir: created directory ‘docs/entrega/tarea-0/AndresHernandez/img/’

$ touch docs/entrega/tarea-0/AndresHernandez/img/.gitkeep
```

+ Copia una imagen en formato `jpg`, `png` o `gif` dentro del directorio `img`.
    - Asegúrate de que la imagen no pese mas de 100KB.

```
$ cp  /ruta/hacia/tonejito.png  docs/entrega/tarea-0/AndresHernandez/img/
$ ls -lA docs/entrega/tarea-0/AndresHernandez/img/tonejito.png
-rw-r--r-- 1 tonejito staff 54549 Sep 28 00:10 docs/entrega/tarea-0/AndresHernandez/img/tonejito.png
```

>>>
**Nota**: El nombre de tu imagen seguramente es diferente, pero debe estar dentro de la capeta `img`.
>>>

--------------------------------------------------------------------------------

### Agregar archivo con tu nombre

+ Crea el archivo `.gitkeep` y un archivo de Markdown que contenga tu nombre
    - Agrega una referencia a la imagen que agregaste en el paso anterior para que se muestre dentro del archivo Markdown
    - Puedes agregar otros campos si gustas

```
$ touch  docs/entrega/tarea-0/AndresHernandez/README.md
$ editor docs/entrega/tarea-0/AndresHernandez/README.md
```

>>>
**Nota**: Reemplaza el comando `editor` con el editor de texto de tu preferencia.
>>>

+ Contenido de ejemplo para el archivo `README.md`

```
# Andrés Hernández

- Número de cuenta: `123456789`

Hola, esta es mi carpeta para la [*tarea-0*](https://redes-ciencias-unam.gitlab.io/2022-1/tareas-so/entrega/tarea-0).

Actividades a las que me quiero dedicar al salir de la facultad:

- Seguridad informática (red team, blue team)
- DevOps (Kubernetes)
- Desarrollo

Cosas que me gusta hacer en mi tiempo libre:

- Dormir con el gato
- Jugar videojuegos
- Ver películas
- Hacer experimentos con componentes electrónicos

| Esta es la imagen que elegí |
|:---------------------------:|
| ![](img/tonejito.png)       |
```

>>>
**Nota**: El nombre de tu imagen seguramente es diferente, pero debe estar dentro de la capeta `img`.
>>>

--------------------------------------------------------------------------------

## Envía los cambios a tu repositorio

+ Una vez que hayas creado los archivos, revisa el estado del repositorio

```
$ git status
On branch AndresHernandez
Untracked files:
  (use "git add <file>..." to include in what will be committed)
	docs/entrega/

nothing added to commit but untracked files present (use "git add" to track)
```

+ Arega los archivos con `git add`

```
$ git add docs/entrega/tarea-0/AndresHernandez/
```

+ Revisa que los archivos hayan sido agregados al _staging area_ utilizando `git status`

```
$ git status
On branch AndresHernandez
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
	new file:   docs/entrega/tarea-0/AndresHernandez/README.md
	new file:   docs/entrega/tarea-0/AndresHernandez/img/.gitkeep
	new file:   docs/entrega/tarea-0/AndresHernandez/img/tonejito.png
```

+ Versiona los archivos con `git commit`

>>>
**Nota**: Usa **comillas simples** para especificar el mensaje del _commit_
>>>

```
$ git commit -m 'Carpeta de Andrés Hernández'
[AndresHernandez cb7fe99] Carpeta de Andrés Hernández
 3 files changed, 22 insertions(+)
 create mode 100644 docs/entrega/tarea-0/AndresHernandez/README.md
 create mode 100644 docs/entrega/tarea-0/AndresHernandez/img/.gitkeep
 create mode 100644 docs/entrega/tarea-0/AndresHernandez/img/tonejito.png
```

+ Revisa que el _remote_ apunte a **tu repositorio** con `git remote`

```
$ git remote -v
origin	https://gitlab.com/USUARIO/tareas-so.git (fetch)
origin	https://gitlab.com/USUARIO/tareas-so.git (push)
```

+ Revisa la rama en la que estas para enviarla a GitLab

```
$ git branch
* AndresHernandez
  entregas
```

+ Envía los cambios a **tu repositorio** utilizando `git push`

```
$ git push -u origin AndresHernandez
Enumerating objects: 12, done.
Counting objects: 100% (12/12), done.
Delta compression using up to 4 threads
Compressing objects: 100% (7/7), done.
Writing objects: 100% (10/10), 53.70 KiB | 13.42 MiB/s, done.
Total 10 (delta 1), reused 1 (delta 0), pack-reused 0
remote: 
remote: To create a merge request for AndresHernandez, visit:
remote:   https://gitlab.com/USUARIO/tareas-so/-/merge_requests/new?merge_request%5Bsource_branch%5D=AndresHernandez
remote: 
To https://gitlab.com/USUARIO/tareas-so.git
 * [new branch]      AndresHernandez -> AndresHernandez
Branch 'AndresHernandez' set up to track remote branch 'AndresHernandez' from 'origin'.
```

--------------------------------------------------------------------------------

## Crea un _merge request_ para entregar tu actividad

Para integrar las tareas de todos se utilizará la funcionalidad `merge request` de GitLab

+ Accede a la url que te apareció en el paso anterior

```
https://gitlab.com/USUARIO/tareas-so/-/merge_requests/new?merge_request%5Bsource_branch%5D=NOMBRE
```

+ Verifica que la rama de destino sea `entregas`
    - Da clic en el botón `Change branches` para ajustar si es necesario

|                                      |
|:------------------------------------:|
| ![](img/009-Fork-MR_branches.png "") |

+ Llena los datos del merge request
    - Escribe un título y una descripción que sirva como vista previa para tu entrega
    - Asegúrate de marcar las siguientes casillas
        * [ ] _Delete source branch when merge request is accepted_.
        * [x] _Squash commits when merge request is accepted_.
+ Da clic en el botón _submit_ para crear tu _merge request_

|                                  |
|:--------------------------------:|
| ![](img/009-Fork-MR_data.png "") |

### Estado del pipeline

Este [repositorio de tareas][repositorio-tareas] cuenta con una configuración de CI/CD que publica el contenido de la carpeta `docs` en [un sitio web][repositorio-vista-web].
Esta operación se realiza mediante un _pipeline_ que construye el sitio y que puede tener uno de 4 estados

| Estado | Acción |
|:------:|:-------|
| <span style="color: gold; font-weight: bold; text-shadow: -1px 1px 0 #000, 1px 1px 0 #000, 1px -1px 0 #000, -1px -1px 0 #000;">pending</span> | Esperar a que el _pipeline_ se ejecute |
| <span style="color: blue; font-weight: bold;">running</span> | Esperar a que el _pipeline_ termine su ejecución |
| <span style="color: green; font-weight: bold;">passed</span> | El trabajo de CI/CD fue exitoso y el _merge request_ podrá ser revisado y aprobado |
| <span style="color: red; font-weight: bold;">failed</span>   | Revisar los mensajes de la bitácora y corregir errores |

>>>
**Nota**: Normalmente los errores del pipeline se dan por archivos que se referencían pero no existen.
Verificar los nombres de archivo y las ligas a imágenes y archivos complementarios.

Para más información consulta el documento [`debug.md`](debug.md)
>>>

--------------------------------------------------------------------------------

##### Notificaciones de creación y seguimiento del _merge request_

+ Una vez que hayas enviado el `merge request`, le llegará un correo electrónico al responsable para que integre tus cambios y podrás visualizar la revisión en la liga
    - <https://gitlab.com/SistemasOperativos-Ciencias-UNAM/2022-1/tareas-so/-/merge_requests>

|                                    |
|:----------------------------------:|
| ![](img/010-Main_MR_created.png "") |

+ Cuando se hayan integrado tus cambios te llegará un correo electrónico de confirmación y aparecerá en el panel del [repositorio principal][repositorio-tareas]

|                                   |
|:---------------------------------:|
| ![](img/012-Main_MR_merged.png "") |

--------------------------------------------------------------------------------

[repositorio-tareas]: https://gitlab.com/SistemasOperativos-Ciencias-UNAM/2022-1/tareas-so.git
[repositorio-vista-web]: https://redes-ciencias-unam.gitlab.io/2022-1/tareas-so/
[gitlab-login]: https://gitlab.com/users/sign_in
[repositorio-personal]: https://gitlab.com/USUARIO/tareas-so
